// 1 Use the count operator to count the total no. of fruits sale

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $count: "fruitsOnSale"  }
])


// 2 use the count operator to count the total no. of fruits with stock more than 20

db.fruits.aggregate([
	{ $match: { stock: { $gte: 20 } } },
	{ $count: "enoughStock" }
])


// 3 use the average operator to  get the average price of  fruits on Sale per unit supplier

db.fruits.aggregate([
	{ $match: { onSale : true } },
	{ $group: { _id: "$supplier_id", avgPrice: { $avg: "$price" } } }
])

// 4 use the max operator to get the highest price of a fruit per supplier

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: { _id: "$supplier_id", max_price: { $max: "$price" } } }
])

// 5 use the min operator to get the lowest price of a fruit per seller

db.fruits.aggregate([
	{ $match: { onSale: true } },
	{ $group: { _id: "$supplier_id", min_price: { $min: "$price" } } }
])